function locScope(){
	var localVar = 2;
	
    if (localVar == 2){
		var localVar = "Ponovno deklariramo varijablu";
		console.log("Unutar if-a localVar = " + localVar);
	}	
	console.log("Izvan if-a localVar = " + localVar);
}

locScope();


//ako deklariramo varijablu pomoću naredbe let, varijable je vidljiva samo unutar 
//tog bloka omeđeno {}
function newLocScope(){
	var localVar = 2;
	
    if (localVar == 2){
		let localVar = "new Ponovno deklariramo varijablu";
		console.log("new Unutar if-a localVar = " + localVar);
	}	
	console.log("new Izvan if-a localVar = " + localVar);
}

newLocScope();

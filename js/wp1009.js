function veciRazlomak() {
    if (a>b){ 
	   console.log("a je veći a = " + a + " b= " + b);
	}else if (a == b){
	   console.log("a = b");	
	}else{
	   console.log("b je veći b= " + b + " a= " + a);
	}
}
//koristimo globalne varijable
//varijable moraju biti definirane prije poziva funkcije inače
//neće raditi.(prebacimo deklaracija a i b varijabli ispod poziva funkcije)
var a = 3/4;
var b = 5/7;

veciRazlomak();

//bolja funkcija koja radi s argumentima

function boljiVeciRazlomak(x,y) {
    if (x>y){ 
	   console.log("Prvi razlomak je veći " + x + " > " + y);
	}else if (x == y){
	   console.log("x = y");	
	}else{
	   console.log("Drugi razlomak je veći " + y + " > " + x);
	}
}

boljiVeciRazlomak(3/4, 5/7);
//na ovaj način funkciju možemo iskoristiti više puta
boljiVeciRazlomak(45/67, 35/45);

//prethodna funkcija samo ispisuje rezulat u konzolu
//što ako s tim rezultatom želimo nešto drugo napraviti?
function josBoljiVeciRazlomak(x,y) {
	var result;
    if (x>y){ 
	   result = "Prvi razlomak je veći " + x + " > " + y;
	}else if (x == y){
	   result = "x = y";	
	}else{
	   result ="Drugi razlomak je veći " + y + " > " + x;
	}
	return result;
}
//kada funkcija vrati rezultat tada s tim rezultatom možemo dalje manipulirati
//npr. možemo rezultate prikazati bilo gdje na stranici
console.log(josBoljiVeciRazlomak(3/4, 5/7));
var rezultat = josBoljiVeciRazlomak(3/4, 5/7);
console.log ("Hm, " + rezultat);

